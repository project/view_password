/**
 * @file
 * Contains \Drupal\view_password\password.js.
 */

(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.pwd = {
    attach(context) {
      const spanClassesCustom = drupalSettings.view_password.span_classes || '';

      const iconExposedCustom = drupalSettings.view_password.icon_exposed || '';
      const iconHiddenCustom = drupalSettings.view_password.icon_hidden || '';

      $(
        once('view_password_button', '.pwd-see [type=password]', context),
      ).after(
        Drupal.theme.showPasswordButton(
          spanClassesCustom,
          drupalSettings.view_password.showPasswordLabel,
        ),
      );
      if (iconHiddenCustom !== '') {
        $(once('view_password_icon_button', '.eye-close', context)).css({
          'background-image': `url(${iconHiddenCustom})`,
        });
      }

      $(once('view_password', '.shwpd', context)).on('click', function () {
        // To toggle the images.
        $(this).toggleClass('eye-close eye-open');
        $(this).removeAttr('style');

        if ($(this).hasClass('eye-open')) {
          $(this).siblings(':password').prop('type', 'text');
          $(this).attr(
            'aria-label',
            drupalSettings.view_password.hidePasswordLabel,
          );
          if (iconExposedCustom !== '') {
            $(this).css({ 'background-image': `url(${iconExposedCustom})` });
          }
        } else if ($(this).hasClass('eye-close')) {
          $(this).siblings(':text').prop('type', 'password');
          $(this).attr(
            'aria-label',
            drupalSettings.view_password.showPasswordLabel,
          );
          if (iconHiddenCustom !== '') {
            $(this).css({ 'background-image': `url(${iconHiddenCustom})` });
          }
        }
      });
    },
  };

  /**
   * Returns the html markup for a 'Show password' button.
   *
   * @param {string} spanClassesCustom
   *   Custom icon classes.
   * @param {string} showPasswordLabel
   *   The human-readable label.
   * @return {string}
   *   The HTML markup for the show password button.
   */
  Drupal.theme.showPasswordButton = (spanClassesCustom, showPasswordLabel) => {
    return `<button type="button" class="shwpd ${spanClassesCustom} eye-close" aria-label="${showPasswordLabel}"></button>`;
  };
})(jQuery, Drupal, drupalSettings, once);
